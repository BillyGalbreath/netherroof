package net.pl3x.bukkit.netherroof;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class NetherRoof extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Bukkit.getPluginManager().registerEvents(this, this);
    }

    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Location to = event.getTo();
        if (!to.getWorld().getName().endsWith("_nether")) {
            return; // not a nether dimension
        }

        Location from = event.getFrom();
        if (to.getBlockX() == from.getBlockX() &&
                to.getBlockY() == from.getBlockY() &&
                to.getBlockZ() == from.getBlockZ() &&
                to.getWorld().getName().equals(from.getWorld().getName())) {
            return; // did not move a full block;
        }

        if (to.getBlockY() < 128) {
            return; // below the top bedrock barrier
        }

        Player player = event.getPlayer();
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("kill-message", "&4You are not allowed to go there.")));
        if (getConfig().getBoolean("kill-roof", true)) {
            player.setHealth(0.0D);
        } else {
            player.teleport(to.getWorld().getSpawnLocation());
        }
    }
}
